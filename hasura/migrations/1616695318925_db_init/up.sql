CREATE TABLE "public"."role"("role" text NOT NULL, PRIMARY KEY ("role") );

CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE TABLE "public"."user"("id" uuid NOT NULL DEFAULT gen_random_uuid(), "email" text NOT NULL, "createdAt" timestamptz NOT NULL DEFAULT now(), "firstName" text NOT NULL, "lastName" text NOT NULL, "imgUrl" text, "role" Text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("email"));

alter table "public"."user"
           add constraint "user_role_fkey"
           foreign key ("role")
           references "public"."role"
           ("role") on update restrict on delete restrict;

CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE TABLE "public"."message"("id" uuid NOT NULL DEFAULT gen_random_uuid(), "userId" uuid NOT NULL DEFAULT gen_random_uuid(), "content" text NOT NULL, "createdAt" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , FOREIGN KEY ("userId") REFERENCES "public"."user"("id") ON UPDATE restrict ON DELETE restrict);
